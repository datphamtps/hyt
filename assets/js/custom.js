const HomePageManager = function () {
};
HomePageManager.prototype = {
    init: function () {
        let that = this;
        this.clickCustomCarouselItem();
        this.resizePageElements();
        this.hoverServiceItem();
        this.playPromoVideo();
        this.fixedMenu();
        $(window).resize(function(){
            that.resizePageElements();
        });
    },
    clickCustomCarouselItem: function () {
        let index = (self) => {
            let p = $(self).parents('.carousel-item')[0];
            return Array.from(p.parentNode.children).indexOf(p);
        };
        $('.carousel-item > *').click(function () {
            $(this).parents('.carousel').find('.carousel-indicators [data-slide-to=' + index(this) + ']').trigger('click');
        });
    },
    resizePageElements: function () {
        // service image
        let serviceImages = Array($('.service-item .item-image'));
        serviceImages.forEach(function (image) {
            image.css('height', image.width());
        });

        // bussiness model
        let bussinessItem = $('.introduce .introduce-item');
        $.each(bussinessItem, function (key, item) {
            item.style.height = item.offsetHeight + 'px';
        });
    },
    hoverServiceItem: function () {
        let item = $('.service-item');
        item.find('.plus').hover(function () {
            $(this).siblings('.item-image').trigger('hover');
        });
    },
    playPromoVideo: function () {
        let promoVideo = $('#promo-video');
        promoVideo.hide();
        $('.promo-button').click(function (e) {
            e.preventDefault();
            promoVideo.trigger('play');
            promoVideo.trigger('requestFullscreen');
            promoVideo.show();
        });

        promoVideo.get(0).addEventListener('fullscreenchange',
            function () {
                if (!document.fullscreenElement) {
                    promoVideo.trigger('pause');
                    promoVideo.hide();
                }
            }, false
        );
    },
    fixedMenu: function () {
        let navbar = $('.navbar');
        $(window).on('scroll', function () {
            let scrollValue = $(window).scrollTop();
            let offsetTop = navbar.attr('data-offset-top');
            offsetTop = parseInt(offsetTop ? offsetTop : 120);
            if (scrollValue > offsetTop) {
                navbar.addClass('fixed-top');
            } else{
                navbar.removeClass('fixed-top');
            }
        });

        let navLink = $('.navbar .navbar-nav.menu .nav-link');
        navLink.click(function (e) {
            navLink.removeClass('active');
            $(this).addClass('active');

            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                let target = $(this.hash);
                if (target.length) {
                    let offset = (navbar.hasClass('fixed-top')) ? target.position().top - navbar.height() : target.position().top - (2*navbar.height());
                    window.scrollTo(0, offset);
                    return false;
                }
            }
        })
    }
};
const HP_MANAGER = new HomePageManager();
HP_MANAGER.init();

new WOW().init();